#!/bin/bash


PYTHON_SCRIPT="skaro_experiment.py"
BASE="test"
TESTS=10


if [ "$#" -eq 0 ]
then
    echo "ERROR: Need the test name"
    exit 1
fi

CONFIG=$1

if [ "$#" -eq 1 ]
then
    echo "WARNING: No number of tests supplied. Using default $TESTS."
else
    TESTS=$2
fi

for ((i=1; i<=$TESTS; i++))
do
    FILE=${BASE}$i
    python $PYTHON_SCRIPT $CONFIG $FILE ${@:3} &
done