import numpy as np

from dataset import DatasetV2, ActionSpace, OutcomeSpace, ProcedureSpace
from utils import save_raw
import copy



# Rules used to build procedural teachers

def rule1(goal):
    p_type = (1, 0)
    pen = np.array([-0.3, 0.5, 1.0])
    return p_type, np.array(pen.tolist() + goal.tolist())


def rule2(goal):
    p_type = (1, 0)
    p1 = [goal[0], goal[1], 1.0]
    p2 = [goal[2], goal[3], -0.1]
    return p_type, np.array(p1 + p2)


def rule4(goal):
    p_type = (4, 0)
    p1 = [0.0, 0.0, 0.0]
    p2 = goal/5.0 + np.array([-0.6, 0.4 , 0.6])
    return p_type, np.array(p1 + p2.tolist())


def rule5(goal):
    p_type = (5, 0)
    p1 = [0.0, 0.0, 0.0]
    p2 = goal/5.0 + np.array([-0.1, -0.5 , 0.6])
    return p_type, np.array(p1 + p2.tolist())


def rule6(goal):
    p_type = (4, 5)
    p1 = [goal[0], 0.0, 0.0]
    p2 = [0.0, goal[1], 0.0]
    return p_type, np.array(p1 + p2)



# Create datasets for teachers
y_options = {'nn_best_locality': 100}
a1 = ActionSpace({'min': -np.ones(13), 'max': np.ones(13)}, 1, "A")

y1 = OutcomeSpace({'min': -np.ones(3), 'max': np.ones(3)}, "Hand Pose")
y2 = OutcomeSpace({'min': -np.ones(3), 'max': np.ones(3)}, "Pen pose")
y3 = OutcomeSpace({'min': -np.ones(4), 'max': np.ones(4)}, "Drawing")
y4 = OutcomeSpace({'min': -np.ones(3), 'max': np.ones(3)}, "Obj pose 0")
y5 = OutcomeSpace({'min': -np.ones(3), 'max': np.ones(3)}, "Joystick pose 0")
y6 = OutcomeSpace({'min': -np.ones(3), 'max': np.ones(3)}, "Joystick pose 1")
y7 = OutcomeSpace({'min': -np.ones(2), 'max': np.ones(2)}, "Player")

data1 = DatasetV2([a1], [y1, y2, y3, y4, y5, y6, y7])
data2 = copy.deepcopy(data1)
data4 = copy.deepcopy(data1)
data5 = copy.deepcopy(data1)
data6 = copy.deepcopy(data1)


# Goals for teachers
"""
# Dense
hor_grid1 = [[-0.9, 0.0],[-0.45, -0.45],[-0.45, 0.45],[0.0, -0.9], \
    [0.0, 0.0],[0.0, 0.9], [0.45, -0.45],[0.45, 0.45],[0.9, 0.0]]
z_grid1 = [0.0, 0.3, 0.6, 0.9]
"""
# Sparse
hor_grid1 = [[-0.9, 0.0],[0.0, -0.9],[0.0, 0.0],[0.0, 0.9],[0.9, 0.0]]
z_grid1 = [0.0, 0.45, 0.9]

goals1 = []
for z in z_grid1:
    for d in hor_grid1:
        p = np.array(d + [z])
        goals1.append(p + np.random.uniform(-0.1, 0.1, 3))

"""
# Dense
hor_grid4 = [[-0.8, -0.8],[-0.8, 0.0],[-0.8, 0.8],[0.0, -0.8],[0.0, 0.0], \
    [0.0, 0.8],[0.8, -0.8],[0.8, 0.0], [0.8, 0.8]]
z_grid4 = [-0.9, -0.3, 0.3, 0.9]
"""
# Sparse
hor_grid4 = [[-0.8, -0.8],[-0.2, 0.0],[-0.8, 0.8], \
    [0.8, -0.8],[0.2, 0.0], [0.8, 0.8]]
z_grid4 = [-0.9, 0.0, 0.9]

goals4 = []
goals5 = []
for z in z_grid4:
    for d in hor_grid4:
        p = np.array(d + [z])
        goals4.append(p + np.random.uniform(-0.1, 0.1, 3))
        goals5.append(p + np.random.uniform(-0.1, 0.1, 3))

"""
# Dense
grid2 = [[-0.9, -0.9],[-0.9, 0.0],[-0.9, 0.9],[0.0, -0.9],[0.0, 0.0], \
    [0.0, 0.9],[0.9, -0.9],[0.9, 0.0],[0.9,0.9]]

goals2 = []
for i, d_i in enumerate(grid2):
    for j, d_j in enumerate(grid2[(i+1):]):
        p = np.array(d_i + d_j)
        goals2.append(p + np.random.uniform(-0.1, 0.1, 4))
"""
# Sparse
grid2 = [[-0.6, -0.6],[-0.6, 0.6],[0.6, -0.6],[0.6, 0.6],[0.0, 0.0]]

goals2 = []
for d_i in grid2:
    for d_j in grid2:
        p = np.array(d_i + d_j)
        goals2.append(p + np.random.uniform(-0.1, 0.1, 4))

"""
# Dense
grid6 = [-0.9, -0.45, 0.0, 0.45, 0.9]
goals6 = []
for x in grid6:
    for y in grid6:
        p = np.array([x, y])
        goals6.append(p + np.random.uniform(-0.1, 0.1, 2))
"""
# Sparse
grid6 = [-0.8, 0.0, 0.8]
goals6 = []
for x in grid6:
    for y in grid6:
        p = np.array([x, y])
        goals6.append(p + np.random.uniform(-0.1, 0.1, 2))


# Create procedures to reach those goals
for g in goals1:
    a = np.zeros(a1.dim)
    cost = 1.0
    p_type, p = rule1(g)
    data1.add_entity(a, (0, 0), [g], [1], cost, p, p_type)

for g in goals2:
    a = np.zeros(a1.dim)
    cost = 1.0
    p_type, p = rule2(g)
    data2.add_entity(a, (0, 0), [g], [2], cost, p, p_type)

for g in goals4:
    a = np.zeros(a1.dim)
    cost = 1.0
    p_type, p = rule4(g)
    data4.add_entity(a, (0, 0), [g], [4], cost, p, p_type)

for g in goals5:
    a = np.zeros(a1.dim)
    cost = 1.0
    p_type, p = rule5(g)
    data5.add_entity(a, (0, 0), [g], [5], cost, p, p_type)

for g in goals6:
    a = np.zeros(a1.dim)
    cost = 1.0
    p_type, p = rule6(g)
    data6.add_entity(a, (0, 0), [g], [6], cost, p, p_type)


# Save teachers
save_raw(data1, "ProceduralTeacher1")
save_raw(data2, "ProceduralTeacher2")
save_raw(data4, "ProceduralTeacher4")
save_raw(data5, "ProceduralTeacher5")
save_raw(data6, "ProceduralTeacher6")








