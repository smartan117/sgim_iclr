import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D








class Visualizer:
    """Implements a visualizer that will call pre-specified plot functions on pre-specified figures."""
    def __init__(self, fig_id, plots):
        """
        fig_id int: the id of the figure on which to plot
        plots list of dict: describe the plot to visualize
        
        Each part of plots correspond to a different subfigure.
        Each part of a same subplot will be plotted on the same subfigure.
        """
        self.fig_id = fig_id
        self.fig = None
        # Each dictionary corresponds to a subplot and contains the following keys
        ## marker: list of the markers used for each plot on the subfigure
        ## color: list of the colors used for each plot on the subfigure
        ## linestyle: list of the linestyles used for each plot on the subfigure
        ## legend: list of all legends for each plot on the subfigure
        ## plots: list of all plot functions to call on the subfigure
        ## axes: list of labels for each axis (optional)
        ## ## each of those functions must have following prototype
        ## ## plot(axis, options) where options is a dictionary containing following keys
        ## ## marker: string, color: string or matplotlib color, linestyle: string   
        self.plots = plots
        n_plots = len(self.plots)
        assert n_plots < 9
        if n_plots > 1:
            n_columns = 2
        else:
            n_columns = 1
        n_rows = (n_plots+1) / 2
        self.subplot = str(n_rows) + str(n_columns)
    
    def add_subplot(self, subplot):
        """Add another subplot to the visualizer."""
        self.plots.append(subplot)
    
    def merge_plots(self, plots, i):
        """Merge the plots with subplot number i."""
        assert i < len(self.plots)
        self.plots[i]['marker'] += plots['marker']
        self.plots[i]['color'] += plots['color']
        self.plots[i]['linestyle'] += plots['linestyle']
        self.plots[i]['legend'] += plots['legend']
        self.plots[i]['plots'] += plots['plots']
        
    def plot(self):
        """Plot the visualizer."""
        self.init_plot()
        # For all subplot of the figure
        for i in range(len(self.plots)):
            # Create new axis for the subplot
            if len(self.plots[i]['limits']['min']) == 3:
                ax = self.add_3daxis(self.subplot + str(i+1))
                leg = []
            else:
                ax = self.add_axis(self.subplot + str(i+1))
            plt.cla()
            
            # Retrieve axis labels
            axes_labels = ['X', 'Y', 'Z']
            if self.plots[i].has_key('axes'):
                axes_labels = self.plots[i]['axes']
            
            # For all plot to draw on this subfigure
            for k in range(len(self.plots[i]['plots'])):
                if len(self.plots[i]['limits']['min']) == 3:
                    self.plots[i]['plots'][k](ax,{'marker': self.plots[i]['marker'][k], 'color': self.plots[i]['color'][k]})
                    # Hack to get a legend on 3D axis subfigures
                    fake2Dline = mpl.lines.Line2D([0],[0], linestyle=self.plots[i]['linestyle'][k], c=self.plots[i]['color'][k], marker = self.plots[i]['marker'][k])
                    leg.append(fake2Dline)
                else:
                    self.plots[i]['plots'][k](ax,{'marker': self.plots[i]['marker'][k], 'color': self.plots[i]['color'][k], 'linestyle': self.plots[i]['linestyle'][k]})
            
            # Set labels for axis dimensions
            if len(self.plots[i]['limits']['min']) >= 1:
                ax.set_xlabel(axes_labels[0])
                if self.plots[i]['limits']['min'][0] != None:
                    ax.set_xlim(self.plots[i]['limits']['min'][0], self.plots[i]['limits']['max'][0])
            if len(self.plots[i]['limits']['min']) >= 2:
                ax.set_ylabel(axes_labels[1])
                if self.plots[i]['limits']['min'][1] != None:
                    ax.set_ylim(self.plots[i]['limits']['min'][1], self.plots[i]['limits']['max'][1])
            if len(self.plots[i]['limits']['min']) >= 3:
                ax.set_zlabel(axes_labels[2])
                if self.plots[i]['limits']['min'][2] != None:
                    ax.set_zlim(self.plots[i]['limits']['min'][2], self.plots[i]['limits']['max'][2])
            
            # Set legend and make it draggable
            if len(self.plots[i]['limits']['min']) == 3:
                if len(self.plots[i]['legend']) > 0:
                    leg = ax.legend(leg, self.plots[i]['legend'], numpoints = 1)
                    leg.draggable()
            else:
                if len(self.plots[i]['legend']) > 0:                    
                    leg = ax.legend(self.plots[i]['legend'])
                    leg.draggable()
            # Set subfigure titles
            ax.set_title(self.plots[i]['title'])
        self.end_plot()

    def init_plot(self):
        """Initialize the figure."""
        plt.ion()
        self.fig = plt.figure(self.fig_id)
        plt.show()
        plt.clf()

    def end_plot(self):
        """Actually draw everything on the figure."""
        plt.draw()

    def add_3daxis(self, subfig):
        """Add another 3D axis to the figure."""
        return self.fig.add_subplot(subfig, projection='3d')

    def add_axis(self, subfig):
        """Add another axis to the figure."""
        return self.fig.add_subplot(subfig)




if __name__ == "__main__":
    
    a = []
    for i in range(100):
        a.append(np.random.uniform(-1.0, 1.0, 2))
    a = np.array(a)
    
    b = []
    for i in range(50):
        b.append(np.random.uniform(0.0, 0.8, 2))
    b = np.array(b)
    
    c = []
    for i in range(100):
        c.append(np.random.uniform(-1.0, 1.0, 3))
    c = np.array(c)
    
    d= []
    for i in range(50):
        d.append(np.random.uniform(0.0, 0.7, 3))
    d = np.array(d)
    
    def plotA(ax, options):
        ax.plot(a[:,0], a[:,1], marker=options['marker'], color=options['color'], linestyle=options['linestyle'])
    
    def plotB(ax, options):
        ax.plot(b[:,0], b[:,1], marker=options['marker'], color=options['color'], linestyle=options['linestyle'])
        
    def plotC(ax, options):
        ax.scatter(c[:,0], c[:,1], c[:,2], marker=options['marker'], color=options['color'])
        
    def plotD(ax, options):
        ax.scatter(d[:,0], d[:,1], d[:,2], marker=options['marker'], color=options['color'])
    
    plot_all = {'limits': {'min': [-1.0, -1.0], 'max': [1.0, 1.0]}, 'title': "Test", 'color': ['r', 'g'], 'marker': ['o', '.'], \
        'linestyle': ['None', 'None'], 'legend': ['A', 'B'], 'plots': [plotA, plotB]}
    
    plot_rest = {'limits': {'min': [-1.0, -1.0, -1.0], 'max': [1.0, 1.0, 1.0]}, 'title': "Test 3D", 'color': ['b', 'k'], 'marker': ['.', '+'], \
        'linestyle': ['None', 'None'], 'legend': ['C', 'D'], 'plots': [plotC, plotD]}
    
    vis = Visualizer(1, [plot_all, plot_rest])
    
    vis.plot()
    
    raw_input()
    
    a = b
    
    vis.plot()





