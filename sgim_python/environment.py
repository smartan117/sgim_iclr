import random
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import math
import time

import copy

from dataset import DatasetV2

import sys
sys.path.insert(0, "../poppycontrol")
import DMP







class EnvironmentV4:
    """Describes the environment of the learner in an experiment."""
    def __init__(self, a_spaces, y_spaces, complex_env=True):
        """
        a_spaces list of ActionSpace: the list of primitive action spaces available to the learner
        y_spaces list of OutcomeSpace: the list of outcome spaces
        """
        self.a_spaces = a_spaces
        self.y_spaces = y_spaces
    
    def to_config(self):
        dico = {}
        dico['type'] = self.__class__.__name__
        dico['a_spaces'] = []
        dico['y_spaces'] = []
        for a in self.a_spaces:
            dico['a_spaces'].append(a.to_config())
        for y in self.y_spaces:
            dico['y_spaces'].append(y.to_config())
        return dico
    
    @classmethod
    def from_config(cls, dico):
        a_spaces = []
        y_spaces = []
        for a_s in dico['a_spaces']:
            a_spaces.append(ActionSpace.from_config(a_s))
        for y_s in dico['y_spaces']:
            y_spaces.append(OutcomeSpace.from_config(y_s))
        return EnvironmentV4(a_spaces, y_spaces)
    
    def reset(self):
        """Function resetting the environment called at the end of each iteration."""
        pass
    
    def normalize_a(self, a, a_type):
        """Make sure the action is within bounds."""
        for i in range(len(a)):
            a[i] = max(min(a[i], self.a_spaces[a_type].bounds['max'][i]), self.a_spaces[a_type].bounds['min'][i])
            
    def normalize_y(self, y, y_type):
        """Make sure the outcome is within bounds."""
        for i in range(len(y)):
            y[i] = max(min(y[i], self.y_spaces[y_type].bounds['max'][i]), self.y_spaces[y_type].bounds['min'][i])
    
    def execute(self, a, a_type):
        """Execute the action and send outcomes reached."""
        self.normalize_a(a, a_type)
        y_list = []
        y_types = []
        for task in range(len(self.y_spaces)):
            ys = self.y_spaces[task]
            y = []
            for i in range(ys.dim-1):
                y.append(random.uniform(ys.bounds['min'][i], ys.bounds['max'][i]))
            y_list.append(np.array(y))
            y_types.append(task)
        return y_list, y_types


###### FIND A WAY TO REMOVE THAT FROM THIS FILE WHILE STILL HAVING OLD DATA BE PICKLABLE

import sys
sys.path.insert(0, "../poppycontrol")
import DMP



class VirtualArm2D:
    """Simulate a 2D robotic arm that can also be translated in z with no kinematic constraints."""
    
    def __init__(self, segments):
        """
        segments list of float: the length of each link of the arm
        """
        self.segments = segments
        self.angles = np.zeros(len(segments))
        self.anchor = np.zeros(2)
        # The robot has a joint at each end of a link
        #  the end effector is also among them
        #  each joint contains the following (x, y, erx, ery)
        #  where er is the output vector of the arm afterthe joint
        self.joints = np.zeros((len(segments)+1, 4))
        self.z = 0.0
        self.compute_end()
    
    def set_anchor(self, anchor):
        """Set the position of the anchor of the robot."""
        self.anchor = anchor
        self.compute_end()
    
    def set_angles(self, angles):
        """Set the angles of the robot."""
        self.angles = angles
        self.compute_end()
    
    def set_z(self, z):
        """set the horizontal plan of the robot z."""
        self.z = z
    
    def compute_end(self):
        """Compute the position of each joint of the robot according to angles."""
        ex = np.array([1.0, 0.0])
        ey = np.array([0.0, 1.0])
        self.end = np.array(self.anchor, copy=True)
        
        for i in range(len(self.angles)):
            # Compute output unitary vectors
            e_r = np.cos(self.angles[i]) * ex + np.sin(self.angles[i]) * ey
            e_theta = -np.sin(self.angles[i]) * ex + np.cos(self.angles[i]) * ey
            ex = e_r
            ey = e_theta
            # Compute next joint position
            self.joints[i,0:2] = self.end
            self.joints[i,2:4] = e_r
            self.end += self.segments[i] * ex
        
        self.joints[-1,0:2] = self.end
    
    def plot(self, options = ['k', 'k.']):
        """Plot the arm in the 2D horizontal plan."""
        plt.plot(self.joints[:,0], self.joints[:,1], options[0])
        plt.plot(self.joints[0:-1,0], self.joints[0:-1,1], options[1])
    
    def plot3d(self, ax, options):
        """Plot the arm in the 3D space."""
        ax.plot(self.joints[:,0], self.joints[:,1], np.ones(len(self.joints)) * self.z, \
            color=options['color'][0], marker=options['marker'][0], linestyle=options['linestyle'][0])
        ax.plot(self.joints[0:-1,0], self.joints[0:-1,1], np.ones(len(self.joints)-1) * self.z, \
            color=options['color'][1], marker=options['marker'][1], linestyle=options['linestyle'][1])


class SceneObject:
    """An object in a 3D scene."""
    def __init__(self, name, x, y, z):
        self.name = name
        self.x = x
        self.y = y
        self.z = z
    
    def plot(self, options='ok'):
        """Plot the object in the horizontal plan."""
        plt.plot(np.array(self.x), np.array(self.y), options)
    
    def plot3d(self, ax, options):
        """Plot the object in the 3D space."""
        ax.plot(np.array([self.x]), np.array([self.y]), np.array([self.z]), \
            color=options['color'], marker=options['marker'], linestyle=options['linestyle'])


class GrabableObject(SceneObject):
    """An object that can be grabed."""
    def __init__(self, name, x, y, z):
        SceneObject.__init__(self, name, x, y, z)
        # Reference to the object currently holding the grabable object
        self.graber = None
        # Indicate the state of the object (some object can be broken)
        self.working = True
    
    def update_state(self):
        """Update the position of the object."""
        # A grabable object changes its position when grabed
        if self.graber != None:
            self.x = self.graber.x
            self.y = self.graber.y
            self.z = self.graber.z
    
    def grab(self, graber):
        """Grab the current object."""
        # The object cannot be grabed by two different objects at the same time."""
        if self.graber == None:
            self.graber = graber
    
    def release(self):
        """Release the object from its graber."""
        self.graber = None


class Joystick(GrabableObject):
    """A specific kind of grabable object that can only be moved inside a cubic space."""
    def __init__(self, name, x, y, z, bounds):
        """
        bounds dict of float arrays: the boundaries of the cubic space where the joystick can be moved
        """
        GrabableObject.__init__(self, name, x, y, z)
        # Contains two keys: min and max
        self.bounds = bounds
        # The given position is the anchor of the joystick (where it returns when released)
        self.anchor = (x, y, z)
    
    def be_released(self):
        """Check whether the joystick is inside its cubic space or not."""
        if self.x < self.bounds['min'][0] or self.x > self.bounds['max'][0]:
            return True
        if self.y < self.bounds['min'][1] or self.y > self.bounds['max'][1]:
            return True
        if self.z < self.bounds['min'][2] or self.z > self.bounds['max'][2]:
            return True
        return False
    
    def update_state(self):
        """Update the position of the joystick."""
        if self.graber != None:
            self.x = self.graber.x
            self.y = self.graber.y
            self.z = self.graber.z
            if self.be_released():
                self.graber.release()
                self.x = self.anchor[0]
                self.y = self.anchor[1]
                self.z = self.anchor[2]


class Graber(SceneObject):
    """Object that can grab others."""
    def __init__(self, name, x, y, z):
        SceneObject.__init__(self, name, x, y, z)
        # Reference to the object currently grabed
        self.grabed = None
    
    def grab(self, obj):
        """Grab an object."""
        if self.grabed == None:
            # If no other object in hand just grab it
            obj.grab(self)
            if obj.graber == self:
                self.grabed = obj
        else: # Another item was grabed
            # Else both objects are destroyed
            #  but old object still in hand (other objects can still be destroyed!!!)
            self.grabed.working = False
            obj.working = False
    
    def release(self):
        """Release the currently grabed object."""
        if self.grabed != None:
            self.grabed.release()
            self.grabed = None


class Scene2D5:
    """The scene describing the environment with an arm, objects, a pen among them."""
    def __init__(self, arm, pen, options, fig_id, obj=None):
        """
        arm VirtualArm2D: the arm of the robot
        pen GrabableObject: pen usable by the robot to draw on the floor
        options dict: options of the scene
        fig_id int: the id of the figure on which to plot the scene (still mandatory)
        obj list of GrabableObject: objects present in the scene
        """
        self.arm = arm
        self.hand = Graber("Hand", self.arm.joints[-1,0], self.arm.joints[-1,1], self.arm.z)
        self.obj = obj
        self.pen = pen
        # Axis limits used when plotting the scene
        self.limits = [-1.5, 1.5, -1.5, 1.5, -1.5, 1.5]
        self.fig_id = fig_id
        # Options used to configure scene
        # z_max: max height of contact with floor
        # z_min: min height of contact with floor
        # z_broke: height where pen will break
        # d_pen: max distance to reach pen
        # d_obj: max distance to reach each object (list)
        # breakable: say if pen breakable or not
        self.options = options
        self.reset()
        
    def reset_state(self, anchor, angles, z, pen, obj=None):
        """Reset the scene to its initial state."""
        # Release whatever object is in the hand
        self.hand.release()
        # Reset the arm
        self.arm.set_anchor(anchor)
        self.arm.set_angles(angles)
        self.arm.set_z(z)
        # Reset the pen
        self.pen.x = pen[0]
        self.pen.y = pen[1]
        self.pen.z = pen[2]
        self.pen.working = True
        if self.obj:
            # Reset every object
            for i in range(len(self.obj)):
                self.obj[i].x = obj[i][0]
                self.obj[i].y = obj[i][1]
                self.obj[i].z = obj[i][2]
                self.obj[i].working = True
        self.reset()
        
    def reset(self):
        """Reset the positions' trackers."""
        self.drawing = []
        self.hand_pos = []
        self.pen_pos = []
        self.obj_pos = []
        if self.obj:
            for obj in self.obj:
                self.obj_pos.append([])
    
    def update(self, plot=True):
        """Update the state of the environment."""
        self.update_hand()
        self.update_pen()
        self.update_obj()
        if plot:
            self.plot()
        
    def update_hand(self):
        """Update the hand and store current position."""
        self.hand.x = self.arm.joints[-1,0]
        self.hand.y = self.arm.joints[-1,1]
        self.hand.z = max(self.arm.z, self.options['z_min'])
        self.hand_pos.append([self.hand.x, self.hand.y, self.hand.z])
    
    def update_obj(self):
        """Update the state and store the current position of each objects."""
        if not self.obj:
            return
        for i, obj in enumerate(self.obj):
            if obj.working:
                dist = math.sqrt((self.hand.x - obj.x)**2 + (self.hand.y - obj.y)**2 + (self.hand.z - obj.z)**2)
                
                if dist < self.options['d_obj'][i] and obj.graber == None:
                    self.hand.grab(obj)
                obj.update_state()
                self.obj_pos[i].append([obj.x, obj.y, obj.z])
            else:
                self.obj_pos[i] = []
    
    def update_pen(self):
        """Update the state of the pen and compute the drawings."""
        if self.pen.working:
            # Check distance hand - pen
            dist = math.sqrt((self.hand.x - self.pen.x)**2 + (self.hand.y - self.pen.y)**2 + (self.hand.z - self.pen.z)**2)
            
            # If distance low enough, grab pen
            if dist < self.options['d_pen'] and self.pen.graber == None:
                self.hand.grab(self.pen)
            self.pen.update_state()
            
            # If pen breakable, in hand and hand to low
            if self.options['breakable'] and self.pen.graber != None and self.arm.z < self.options['z_broke']:
                self.pen.working = False
            
            # If pen grabed and touching the floor
            if self.pen.graber != None and self.arm.z < self.options['z_max'] and self.pen.working:
                self.drawing.append([self.pen.x, self.pen.y])
            else:
                self.drawing.append(None)
            
            self.pen_pos.append([self.pen.x, self.pen.y, self.pen.z])
        else:
            self.pen_pos = []
    
    ##### All following plot functions are obsolete
    
    def plot_drawings(self, options):
        """Plot the last continuous drawing recorded in 2D."""
        i = 0
        while i < len(self.drawing):
            while i < len(self.drawing) and self.drawing[i] == None:
                i += 1
            p = []
            while i < len(self.drawing) and self.drawing[i] != None:
                p.append(self.drawing[i])
                i += 1
            if len(p) > 0:
                p = np.array(p)
                plt.plot(p[:,0], p[:,1], options)
    
    def plot_drawings3d(self, ax, options):
        """Plot the last continuous drawing recorded in 3D."""
        i = 0
        while i < len(self.drawing):
            while i < len(self.drawing) and self.drawing[i] == None:
                i += 1
            p = []
            while i < len(self.drawing) and self.drawing[i] != None:
                p.append(self.drawing[i])
                i += 1
            if len(p) > 0:
                p = np.array(p)
                ax.plot(p[:,0], p[:,1], -0.2 * ones(len(p)), color=options['color'], marker=options['marker'], linestyle=options['linestyle'])
    
    def plot_hand_pos(self, n=100, options='k.'):
        """Plot the trace of the recent hand positions."""
        l = min(n, len(self.hand_pos))
        p = np.array(self.hand_pos[(len(self.hand_pos)-l):len(self.hand_pos)])
        plt.plot(p[:,0], p[:,1], options)
    
    def plot_hand_pos3d(self, ax, options, n=100):
        """Plot the trace of the recent hand positions in 3D."""
        l = min(n, len(self.hand_pos))
        p = np.array(self.hand_pos[(len(self.hand_pos)-l):len(self.hand_pos)])
        ax.plot(p[:,0], p[:,1], p[:,2], color=options['color'], marker=options['marker'], linestyle=options['linestyle'])
    
    def plot_pen_pos(self, n=100, options='k.'):
        """Plot the trace of the recent pen positions."""
        l = min(n, len(self.pen_pos))
        p = np.array(self.pen_pos[(len(self.pen_pos)-l):len(self.pen_pos)])
        plt.plot(p[:,0], p[:,1], options)
    
    def plot_pen_pos3d(self, ax, options, n=100):
        """Plot the trace of the recent pen positions in 3D."""
        l = min(n, len(self.pen_pos))
        p = np.array(self.pen_pos[(len(self.pen_pos)-l):len(self.pen_pos)])
        ax.plot(p[:,0], p[:,1], p[:,2], color=options['color'], marker=options['marker'], linestyle=options['linestyle'])
    
    def plot(self):
        """Plot the scene in 2D."""
        plt.ion()
        plt.figure(self.fig_id)
        plt.show()
        plt.cla()
        self.plot_hand_pos()
        self.plot_pen_pos(options='r.')
        #self.plot_obj_pos(options='g.')
        self.arm.plot()
        self.hand.plot('ok')
        #self.obj.plot('og')
        if self.pen.working:
            self.pen.plot('or')
        else:
            self.pen.plot('xr')
        self.plot_drawings('b')
        plt.axis(self.limits[0:4])
        plt.draw()
    
    def plot3d(self):
        """Plot the scene in 3D."""
        plt.ion()
        fig = plt.figure(self.fig_id)
        plt.show()
        plt.clf()
        ax = fig.add_subplot(111, projection='3d')
        self.plot_hand_pos3d(ax, {'color': 'k', 'marker': '.', 'linestyle': 'None'})
        self.plot_pen_pos3d(ax, {'color': 'r', 'marker': '.', 'linestyle': 'None'})
        self.arm.plot3d(ax, {'color': ['k', 'k'], 'marker': ['', '.'], 'linestyle': ['solid','None']})
        self.hand.plot3d(ax, {'color': 'k', 'marker': 'o', 'linestyle': 'None'})
        if self.pen.working:
            self.pen.plot3d(ax, {'color': 'r', 'marker': 'o', 'linestyle': 'None'})
        else:
            self.pen.plot3d(ax, {'color': 'r', 'marker': 'x', 'linestyle': 'None'})
        self.plot_drawings3d(ax, {'color': 'b', 'marker': '', 'linestyle': 'solid'})
        ax.set_xlim(self.limits[0], self.limits[1])
        ax.set_ylim(self.limits[2], self.limits[3])
        ax.set_zlim(self.limits[4], self.limits[5])
        plt.draw()


class ComplexArmEnvironmentV5(EnvironmentV4):
    """Environment containing a scene with a virtual arm controllable by joint angular DMPs."""
    def __init__(self, a_spaces, y_spaces, scene, initial_pose, show=True):
        """
        a_spaces list of ActionSpace: list of all primitive action spaces available
        y_spaces list of OutcomeSpace: list of all outcome spaces in the environment
        scene Scene2D5: the scene containing the environment's state
        initial_pose float array: the position of the anchor of the virtual arm at each reset
        show boolean: indicates whether the scene will be plotted or not (TO NOT USE WHILE PLOT FUNCTIONS HAVEN'T BEEN UPDATED!!!)
        """
        EnvironmentV4.__init__(self, a_spaces, y_spaces)
        self.scene = scene
        self.initial_pose = initial_pose
        self.show = show
        # Contains all the objects initial position (for resetting the scene)
        self.obj = []
        for obj in self.scene.obj:
            self.obj.append([obj.x, obj.y, obj.z])
        self.obj = np.array(self.obj)
    
    def reset(self):
        """Reset the environment."""
        self.reset_state(np.array([0.0, 0.0]), copy.deepcopy(self.initial_pose), 0.0, np.array([-0.3, 0.5, 1.0]), \
                self.obj)
    
    def reset_state(self, anchor, angles, z, pen, obj):
        """Reset the state of the scene."""
        self.scene.reset_state(anchor, angles, z, pen, obj)
    
    def a_to_dmp(self, a, a_type):
        """Extract DMP parameters from a primitive action."""
        assert a_type == 0
        n_bfs = 3
        cs = DMP.CanonicalSystem()
        dmp = DMP.DynamicSystem(cs=cs, tau=5.0)
        
        for j in range(3):
            ts = DMP.TransformationSystem(str(j), n_bfs=n_bfs, start=0., goal=np.pi * a[(n_bfs+1)*j + n_bfs], method="2012")
            weights = []
            for k in range(n_bfs):
                weights.append(100.0*a[(n_bfs+1)*j + k])
            c, h = ts.compute_bfs()
            ts.parameters = DMP.WeightedLinearRegression(c, h, np.array(weights))
            dmp.add_dmp(ts)
        z = a[len(a)-1]
        
        return dmp, z
    
    def execute_dmp(self, dmp, z):
        """Execute the DMP on the arm."""
        real_start = {}
        real_start["0"] = self.scene.arm.angles[0]
        real_start["1"] = self.scene.arm.angles[1]
        real_start["2"] = self.scene.arm.angles[2]
        dmp.set_start(real_start)
        dmp.reset()
        dt = 0.01
        if self.show:
            dt = 0.05
        
        while dmp.cs.t < 5.0:
            self.scene.arm.set_angles(np.array(dmp.get_state(["0", "1", "2"])))
            self.scene.arm.set_z(z)  # z >= 0.0
            self.scene.update(plot=self.show)
            if self.show:
                time.sleep(dt)
            dmp.step(dt)
    
    def compute_drawing(self):
        """Compute every pieces of drawing."""
        drawings = []
        i = 0
        while i < len(self.scene.drawing):
            while i < len(self.scene.drawing) and self.scene.drawing[i] == None:
                i += 1
            if i < len(self.scene.drawing):
                drawings.append([])
            while i < len(self.scene.drawing) and self.scene.drawing[i] != None:
                drawings[-1].append(self.scene.drawing[i])
                i += 1
        return drawings
    
    def execute(self, a, a_type):
        """Execute an action, given context and return them normalized plus the outcomes."""
        assert a_type < len(self.a_spaces)
        assert self.a_spaces[a_type].dim == len(a)
        
        # Create DMP from action
        self.normalize_a(a, a_type)
        dmp, z = self.a_to_dmp(a, a_type)
        
        # Execute DMP
        self.execute_dmp(dmp, z)
        
        # Retrieve outcomes
        y_list = []
        y_types = []
        
        y_hand = self.scene.hand_pos[-1]
        self.normalize_y(y_hand, 0)
        y_list.append(np.array(y_hand))
        y_types.append(0)
        
        if self.scene.pen.graber != None and self.scene.pen.working:
            y_pen = self.scene.pen_pos[-1]
            self.normalize_y(y_pen, 1)
            y_list.append(np.array(y_pen))
            y_types.append(1)
        
        for i, obj in enumerate(self.scene.obj):
            if obj.graber != None and obj.working:
                y_obj = np.array(self.scene.obj_pos[i][-1])
                if isinstance(obj, Joystick):
                    y_obj = (y_obj - obj.bounds['min']) * 2.0 / (obj.bounds['max'] - obj.bounds['min']) - 1.0
                self.normalize_y(y_obj, 3+i)
                y_list.append(y_obj)
                y_types.append(3+i)
        
        drawings = self.compute_drawing()
        if len(drawings) > 0 and self.scene.pen.working:
            y_draw = np.zeros(4)
            y_draw[0:2] = drawings[-1][0]
            y_draw[2:4] = drawings[-1][-1]
            self.normalize_y(y_draw, 2)
            y_list.append(y_draw)
            y_types.append(2)
        
        return y_list, y_types
