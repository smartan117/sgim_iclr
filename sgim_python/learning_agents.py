import numpy as np
import random
import copy
import math

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from interest_models import InterestRegion, InterestTree, InterestModelV3

from utils import uniform_sampling, save_json, save_str, load_json, load_str


import time





class Mod:
    """Implements the SGIM sampling mods."""
    name = "Random"
    def __init__(self, interest_model, prob):
        """
        sample func: function called when sampling with this mod
        prob float: probability of this mod to occur
        """
        self.prob = prob
        self.interest_model = interest_model
    
    def to_config(self):
        dico = {}
        dico['name'] = self.__class__.__name__
        dico['prob'] = self.prob
        return dico
    
    def sample(self):
        return self.interest_model.sample_random()


class GoodRegionMod(Mod):
    """Mod corresponding to SGIM Mod 1."""
    name = "Good Region"
    def sample(self):
        return self.interest_model.sample_good_point()


class GoodPointMod(Mod):
    """Mod corresponding to SGIM Mod 2."""
    name = "Good Point"    
    def sample(self):
        return self.interest_model.sample_best_point()


#### TO DO add iterators to traverse correctly episodes and such
class LearningAgentV4:
    """Default LearningAgent, learns by episode but without any choice of task and goal, choose strategy randomly."""
    
    def __init__(self, dataset, strat, env, verbose=False):
        """
        dataset DatasetV2: dataset of the agent
        strat StrategyV4 list: list of learning strategies available to the agent
        env EnvironmentV4: environment of the experiment
        """
        self.dataset = dataset
        self.strategies = strat
        self.environment = env
        self.learning_process = []
        self.epmem_length = [0]  # Track the correspondence between iteration and episodic memory length (necessary for complex actions)
        self.n_iter = 0
        self.verbose = verbose
    
    def to_config(self):
        dico = {}
        dico['type'] = self.__class__.__name__
        dico['strategies'] = []
        for s in self.strategies:
            dico['strategies'].append(s.to_config())
        dico['dataset'] = {}
        dico['dataset']['options'] = self.dataset.options
        dico['dataset']['type'] = self.dataset.__class__.__name__
        return dico
    
    def _replayer_lp(self):
        i = 0
        while i < len(self.learning_process):
            yield self.learning_process[i]
            i += 1
    
    def _replayer_epmem_range(self, first, last):
        i = first
        while i < last:
            yield self.epmem_length[i]
            i += 1
    
    def _replayer_all_epmem(self):
        i = 0
        j = 0
        while i < len(self.learning_process):
            lp = self.learning_process[i]
            n_iter = lp[0]
            for epmem in self._replayer_epmem_range(j, n_iter):
                yield epmem
            j = n_iter
            i += 1
    
    def _replayer_idE_range(self, first, last):
        i = first
        while i < last:
            yield i
            i += 1
    
    def _replayer_all_idE(self):
        i = 0
        j = 0
        k = 0
        while i < len(self.learning_process):
            lp = self.learning_process[i]
            n_iter = lp[0]
            while j < n_iter:
                size = self.epmem[j]
                for ide in self._replayer_idE_range(k, size):
                    yield ide
                k = size
                j += 1
            i += 1
    
    def _replayer_all_idE_test(self):
        i = 0
        j = 0
        k = 0
        while i < len(self.learning_process):
            lp = self.learning_process[i]
            n_iter = len(self.epmem_length)-1
            if i < len(self.learning_process) - 1:
                n_iter = self.learning_process[i+1][0]
            while j <= n_iter:
                size = self.epmem_length[j]
                for ide in range(k, size):
                    yield i, ide
                k = size
                j += 1
            i += 1
    
    def run(self, n_iter):
        """Run the learner until max number of iterations."""
        while self.n_iter < n_iter:
            # Choose learning strategy randomly
            strat = random.randint(0, len(self.strategies)-1)
            if self.verbose:
                print self.n_iter
                print("Strategy used: " + str(strat))
            # Add choices to the learning process (same shape as the more advance learners)
            self.learning_process.append([self.n_iter, -1, strat, -1, []])
            # Run one learning episode
            self.episode(strat)
        
    def episode(self, strat):
        """Run one learning episode."""
        
        # Run an episode of the given strategy
        self.strategies[strat].run()
        
        # For each strategy iterations
        for m in self.strategies[strat].memory:
            # For each step of the complex action
            for x in m:
                # x on the form [a, atype, y_list, ytypes]
                if self.strategies[strat].complex_actions == 0:
                    self.dataset.create_a_space(x[1])
                if len(x) > 4:
                    # A procedure has been tried
                    self.dataset.add_entity(x[0], x[1], x[2], x[3], \
                            self.dataset.a_spaces[x[1][0]][x[1][1]].options['cost'], x[4], x[5])
                else:
                    self.dataset.add_entity(x[0], x[1], x[2], x[3], \
                            self.dataset.a_spaces[x[1][0]][x[1][1]].options['cost'])
            self.epmem_length.append(len(self.dataset.idA))
            
        # Increment number of iterations by the number of actions performed
        self.n_iter += self.strategies[strat].n
    
    def plot_strategy(self, strat, window, ax, options):
        """Plot the choices of strategy over time."""
        t = np.array(range(self.n_iter))
        strat_choice = []
        last_first_j = 0
        for i in range(self.n_iter):
            choice = np.zeros(len(self.strategies))
            for it in self.learning_process[last_first_j:]:
                if it[0] <= i - window:
                    last_first_j += 1
                elif it[0] < i + window:
                    choice[it[2]] += 1
                else:
                    break
            norm = np.sum(choice)
            if norm > 0:
                choice = choice / norm
            strat_choice.append(choice)
        
        strat_choice = np.array(strat_choice)
        ax.plot(t, strat_choice[:,strat], marker=options['marker'], color=options['color'], linestyle=options['linestyle'])
    
    def plot_task_strategy_points(self, task, strat, ax, options):
        """Plot each reached outcomes of the given task with a color indicating the strategy used to acquire it."""
        points = []
        n = 0
        for i in range(len(self.learning_process)):
            s = self.learning_process[i][2]
            if s == strat:
                n0 = self.epmem_length[self.learning_process[i][0]]
                if i < len(self.learning_process)-1:
                    n1 = self.epmem_length[self.learning_process[i+1][0]]
                else:
                    n1 = len(self.dataset.idY)
                while n < len(self.dataset.y_spaces[task].ids) and self.dataset.y_spaces[task].ids[n] < n0:
                    n += 1
                while n < len(self.dataset.y_spaces[task].ids) and self.dataset.y_spaces[task].ids[n] < n1:
                    points.append(self.dataset.y_spaces[task].data[n])
                    n += 1
        if self.dataset.y_spaces[task].dim < 4:
            p = np.array(points)
            if self.dataset.y_spaces[task].dim == 1:
                ax.plot(p, np.zeros(len(p)), marker=options['marker'], color=options['color'], linestyle=options['linestyle'])
            elif self.dataset.y_spaces[task].dim == 2:
                ax.plot(p[:,0], p[:,1], marker=options['marker'], color=options['color'], linestyle=options['linestyle'])
            elif self.dataset.y_spaces[task].dim == 3:
                try:
                    ax.scatter(p[:,0], p[:,1], p[:,2], marker=options['marker'], color=options['color'])
                except IndexError:
                    print p
    
    #### The following functions are here ro ease the use of Visualizers
    
    def get_strategies_visualizer(self, window, prefix=""):
        """Return a dictionary used to visualize the choices of strategy."""
        dico = {}
        cmap = plt.cm.jet
        colors = []
        markers = []
        lines = []
        legends = []
        plots = []
        for i in range(len(self.strategies)):
            if len(self.strategies) == 1:
                colors.append('k')
            else:
                colors.append(cmap(int(math.floor(float(i)*float(cmap.N - 1)/float(len(self.strategies)-1)))))
            markers.append('')
            lines.append('solid')
            legends.append(self.strategies[i].name)
            plots.append(lambda ax, options, i=i: self.plot_strategy(i, window, ax, options))
        
        dico['limits'] = {'min': [None, 0.0], 'max': [None, 1.0]}
        dico['title'] = prefix + "Strategy choice"
        dico['color'] = colors
        dico['marker'] = markers
        dico['linestyle'] = lines
        dico['legend'] = legends
        dico['axes'] = ['Iterations', '%']
        dico['plots'] = plots
        
        return dico
    
    def get_strategies_points_visualizer(self, task, prefix=""):
        """Return a dictionary used to visualize the points reached by a given strategy."""
        dico = {}
        cmap = plt.cm.jet
        colors = []
        markers = []
        lines = []
        legends = []
        plots = []
        for i in range(len(self.strategies)):
            if len(self.strategies) == 1:
                colors.append('k')
            else:
                colors.append(cmap(int(math.floor(float(i)*float(cmap.N - 1)/float(len(self.strategies)-1)))))
            markers.append('.')
            lines.append('None')
            legends.append(self.strategies[i].name)
            plots.append(lambda ax, options, i=i: self.plot_task_strategy_points(task, i, ax, options))
        
        dico['limits'] = {'min': self.dataset.y_spaces[task].bounds['min'], \
            'max': self.dataset.y_spaces[task].bounds['max']}
        dico['title'] = prefix + "Points reached task " + str(task)
        dico['color'] = colors
        dico['marker'] = markers
        dico['linestyle'] = lines
        dico['legend'] = legends
        dico['plots'] = plots
        
        return dico
    

class InterestAgentV4(LearningAgentV4):
    """Complete learning agent with an interest model and mods."""
    
    def __init__(self, dataset, strat, mods, interest_model, env, verbose=False):
        """
        dataset DatasetV2: dataset of the agent
        strat StrategyV4 list: list of learning strategies available to the agent
        mods Mod list: list of available mods (used to determine wich task/goal/strategy to explore
        interest_model InterestModelV3: interest model used by the agent to guide its learning process
        env EnvironmentV4: environment of the experiment
        """
        LearningAgentV4.__init__(self, dataset, strat, env, verbose)
        self.mods = mods
        self.interest_model = interest_model
        self.progresses = []
        self.goal_progresses = []
    
    def to_config(self):
        dico = LearningAgentV4.to_config(self)
        dico['mods'] = []
        for m in self.mods:
            dico['mods'].append(m.to_config())
        dico['interest_model'] = self.interest_model.to_config()
        return dico
    
    def replay(self):
        # For each learning episode
        for i, lp in enumerate(self.learning_process):
            first_it = lp[0]
            mod = lp[1]
            strat = lp[2]
            task = lp[3]
            goal = lp[4]
            print("Learner chose with mod " + str(mod) + ": " + self.dataset.y_spaces[task].name + \
                    " (" + str(goal) + ") with strategy " + self.strategies[strat].name)
            if i < len(self.learning_process) - 1:
                last_it = self.learning_process[i+1][0]
            else:
                last_it = len(self.epmem_length)-1
            # For each iteration of the learning episode
            for it in range(first_it, last_it):
                firstidE = self.epmem_length[it]
                if it < len(self.epmem_length)-1:
                    lastidE = self.epmem_length[it+1]
                else:
                    lastidE = len(self.dataset.idY)
                # For each sub-action recorded
                for j, idE in enumerate(range(firstidE, lastidE)):
                    pass
        
    def run(self, n_iter):
        """Run the learner until max number of iterations."""
        while self.n_iter < n_iter:
            # Select stochastically mod
            mod = self.choose_mod()
            # Select task, strategy and goal outcome according to chosen mod
            task, strat, goal = self.choose_strat_goal(mod)
            # Make sure strategy chosen is available
            while not self.strategies[strat].available(task):
                task, strat, goal = self.choose_strat_goal(mod)
            if self.verbose:
                print("Iterations: " + str(self.n_iter))
                print("Mod " + str(mod) + " chose space " + str(task) + ", strategy " + str(strat) + " and goal " + str(goal))
            # Add choices to the learning process (same shape as the less advance learners)
            self.learning_process.append([self.n_iter, mod, strat, task, goal])
            # Run one episode
            self.episode(strat, goal, task)
        
    def episode(self, strat, goal, task):
        """Run one episode of given strategy to reach the given goal in given task space."""
        
        # Run the episode
        self.strategies[strat].run_goal(goal, task)
        
        # Compute previous goal competence
        goal_comp = self.dataset.competence(goal, task)
        
        list_points = []
        
        # For each strategy iterations
        for m in self.strategies[strat].memory:
            # For each step of the complex action
            all_progresses = []
            for x in m:
                # x on the form [a, atype, y_list, ytypes]
                all_progresses.append([])
                old_comp_list = []
                for y, t in zip(x[2], x[3]):
                    old_comp_list.append(self.dataset.competence(y, t))
                if self.strategies[strat].complex_actions == 0:
                    self.dataset.create_a_space(x[1])
                if len(x) > 4:
                    # A procedure has been tried
                    self.dataset.add_entity(x[0], x[1], x[2], x[3], \
                            self.dataset.a_spaces[x[1][0]][x[1][1]].options['cost'], x[4], x[5])
                else:
                    self.dataset.add_entity(x[0], x[1], x[2], x[3], \
                            self.dataset.a_spaces[x[1][0]][x[1][1]].options['cost'])
                for y, t, old_comp in zip(x[2], x[3], old_comp_list):
                    new_comp = self.dataset.competence(y, t)
                    progress = self.progress(old_comp, new_comp)
                    # Add point in interest region
                    self.interest_model.add_point(y, t, strat, progress)
                    all_progresses[-1].append(progress)
            self.progresses.append(all_progresses)
            self.epmem_length.append(len(self.dataset.idA))
        
        # Compute goal progress and add goal to interest region        
        new_goal_comp = self.dataset.competence(goal, task)
        goal_progress = self.progress(goal_comp, new_goal_comp)
        self.interest_model.add_point(goal, task, strat, goal_progress)
        self.goal_progresses.append(goal_progress)
        
        # Increment number of iterations
        self.n_iter += self.strategies[strat].n
    
    def compute_progress(self, y, task, before):
        """Compute progress in reaching a given target between now and before."""
        old_comp = self.dataset.competence(y, task, n_iter=before)
        new_comp = self.dataset.competence(y, task)
        #print("Task " + str(task) + ", point " + str(y) + ", comp(" + str(before) + "): " + str(old_comp) + " -> " + str(new_comp))
        return self.progress(old_comp, new_comp)   
        
    def choose_mod(self):
        """Choose mod stochastically."""
        probs = []
        for m in self.mods:
            probs.append(m.prob)
        return uniform_sampling(probs)
    
    def choose_strat_goal(self, mod):
        """Sample strategy and goal according to chosen mod."""
        return self.mods[mod].sample()
        
    @staticmethod
    def progress(old_comp, new_comp):
        """Static method returning progress computed with old and new competence."""
        return new_comp - old_comp
    
    def compute_strategy_task(self):
        """Return a matrix giving choices per task (rows) and strategies (columns)."""
        choices = np.zeros((len(self.dataset.y_spaces), len(self.strategies)), dtype=np.float64)
        for it in self.learning_process:
            if it[1] > 0:
                choices[it[3], it[2]] += 1
        #by_task = np.sum(choices, axis=1)
        #by_task = np.reshape(by_task, (len(self.dataset.y_spaces), 1))
        #choices /= by_task
        return choices
    
    def plot_tasks(self, task, window, ax, options): # Careful, removing mod 0
        """Plot the choices of task over time."""
        t = np.array(range(self.n_iter))
        task_choice = []
        last_first_j = 0
        for i in range(self.n_iter):
            choice = np.zeros(len(self.interest_model.interest_trees))
            for it in self.learning_process[last_first_j:]:
                if it[0] <= i - window:
                    last_first_j += 1
                elif it[0] < i + window:
                    choice[it[3]] += 1
                else:
                    break
            norm = np.sum(choice)
            if norm > 0:
                choice = choice / norm 
            task_choice.append(choice)
        
        task_choice = np.array(task_choice)
        ax.plot(t, task_choice[:,task], marker=options['marker'], color=options['color'], linestyle=options['linestyle'])
    
    def plot_goals_task(self, task, ax, options): # Careful, removing mod 0
        """Plot the goals chosen for a given task space."""
        goals = []
        if self.dataset.y_spaces[task].dim < 4:
            for it in self.learning_process:
                if it[3] == task and it[1] > 0:
                    goals.append(it[4])
            goals = np.array(goals)
            if len(goals) > 0:
                if self.dataset.y_spaces[task].dim == 1:
                    ax.plot(goals, np.zeros(len(goals)), marker=options['marker'], color=options['color'], linestyle=options['linestyle'])
                elif self.dataset.y_spaces[task].dim == 2:
                    ax.plot(goals[:,0], goals[:,1], marker=options['marker'], color=options['color'], linestyle=options['linestyle'])
                elif self.dataset.y_spaces[task].dim == 3:
                    ax.scatter(goals[:,0], goals[:,1], goals[:,2], marker=options['marker'], color=options['color'])
    
    def plot_task_strategy_goal(self, task, strat, ax, options): # Careful, removing mod 0
        """Plot the goals chosen for a specific task space and strategy."""
        goals = []
        if self.dataset.y_spaces[task].dim < 4:
            for it in self.learning_process:
                if it[3] == task and it[2] == strat and it[1] > 0:
                    goals.append(it[4])
            goals = np.array(goals)
            if len(goals) > 0:
                if self.dataset.y_spaces[task].dim == 1:
                    ax.plot(goals, np.zeros(len(goals)), marker=options['marker'], color=options['color'], linestyle=options['linestyle'])
                elif self.dataset.y_spaces[task].dim == 2:
                    ax.plot(goals[:,0], goals[:,1], marker=options['marker'], color=options['color'], linestyle=options['linestyle'])
                elif self.dataset.y_spaces[task].dim == 3:
                    ax.scatter(goals[:,0], goals[:,1], goals[:,2], marker=options['marker'], color=options['color'])
    
    def plot_task_mod_goal(self, task, mod, ax, options):
        """Plot the goals chosen for a specific task space and by a specific mod."""
        goals = []
        if self.dataset.y_spaces[task].dim < 4:
            for it in self.learning_process:
                if it[3] == task and it[1] == mod:
                    goals.append(it[4])
            goals = np.array(goals)
            if len(goals) > 0:
                if self.dataset.y_spaces[task].dim == 1:
                    ax.plot(goals, np.zeros(len(goals)), marker=options['marker'], color=options['color'], linestyle=options['linestyle'])
                elif self.dataset.y_spaces[task].dim == 2:
                    ax.plot(goals[:,0], goals[:,1], marker=options['marker'], color=options['color'], linestyle=options['linestyle'])
                elif self.dataset.y_spaces[task].dim == 3:
                    ax.scatter(goals[:,0], goals[:,1], goals[:,2], marker=options['marker'], color=options['color'])
    
    def plot_mod(self, mod, window, ax, options):
        """Plot the mod chosen over time."""
        t = np.array(range(self.n_iter))
        mod_choice = []
        for i in range(self.n_iter):
            choice = np.zeros(len(self.mods))
            for it in self.learning_process:
                if it[0] > i - window and it[0] < i + window:
                    choice[it[1]] += 1
                if it[0] >= i + window:
                    break
            norm = np.sum(choice)
            if norm > 0:
                choice = choice / norm 
            mod_choice.append(choice)
        
        mod_choice = np.array(mod_choice)
        ax.plot(t, mod_choice[:,mod], marker=options['marker'], color=options['color'], linestyle=options['linestyle'])
    
    #### The following functions are here ro ease the use of Visualizers
    
    def get_tasks_visualizer(self, window, prefix=""):
        """Return a dictionary used to visualize the choices of tasks."""
        dico = {}
        cmap = plt.cm.jet
        colors = []
        markers = []
        lines = []
        legends = []
        plots = []
        for i in range(len(self.interest_model.interest_trees)):
            if len(self.interest_model.interest_trees) == 1:
                colors.append('k')
            else:
                colors.append(cmap(int(math.floor(float(i)*float(cmap.N - 1)/float(len(self.interest_model.interest_trees)-1)))))
            markers.append('')
            lines.append('solid')
            legends.append("Task " + str(i))
            plots.append(lambda ax, options, i=i: self.plot_tasks(i, window, ax, options))
        
        dico['limits'] = {'min': [None, 0.0], 'max': [None, 1.0]}
        dico['title'] = prefix + "Task choice"
        dico['color'] = colors
        dico['marker'] = markers
        dico['linestyle'] = lines
        dico['axes'] = ['Iterations', '%']
        dico['legend'] = legends
        dico['plots'] = plots
        
        return dico
    
    def get_mods_visualizer(self, window, prefix=""):
        """Return a dictionary used to visualize the choices of mod."""
        dico = {}
        cmap = plt.cm.jet
        colors = []
        markers = []
        lines = []
        legends = []
        plots = []
        for i in range(len(self.mods)):
            if len(self.mods) == 1:
                colors.append('k')
            else:
                colors.append(cmap(int(math.floor(float(i)*float(cmap.N - 1)/float(len(self.mods)-1)))))
            markers.append('')
            lines.append('solid')
            legends.append(self.mods[i].name)
            plots.append(lambda ax, options, i=i: self.plot_mod(i, window, ax, options))
        
        dico['limits'] = {'min': [None, 0.0], 'max': [None, 1.0]}
        dico['title'] = prefix + "Mod choice"
        dico['color'] = colors
        dico['marker'] = markers
        dico['axes'] = ['Iterations', '%']
        dico['linestyle'] = lines
        dico['legend'] = legends
        dico['plots'] = plots
        
        return dico
    
    def get_goals_visualizer(self, task, prefix=""):
        """Return a dictionary used to visualize the goals of a specific task space."""
        dico = {}
        dico['limits'] = {'min': self.dataset.y_spaces[task].bounds['min'], \
            'max': self.dataset.y_spaces[task].bounds['max']}
        dico['title'] = prefix + "Goals chosen task " + str(task)
        dico['color'] = ['k']
        dico['marker'] = ['.']
        dico['linestyle'] = ['None']
        dico['legend'] = []
        dico['plots'] = [lambda ax, options: self.plot_goals_task(task, ax, options)]
        
        return dico
    
    def get_goals_strategy_visualizer(self, task, prefix=""):
        """Return a dictionary used to visualize the goals chosen for a specific task space sorted by strategy."""
        cmap = plt.cm.jet
        colors = []
        markers = []
        lines = []
        legends = []
        plots = []
        for i in range(len(self.strategies)):
            if len(self.strategies) == 1:
                colors.append('k')
            else:
                colors.append(cmap(int(math.floor(float(i)*float(cmap.N - 1)/float(len(self.strategies)-1)))))
            markers.append('.')
            lines.append('None')
            legends.append(self.strategies[i].name)
            plots.append(lambda ax, options, i=i: self.plot_task_strategy_goal(task, i, ax, options))
        
        dico = {}
        dico['limits'] = {'min': self.dataset.y_spaces[task].bounds['min'], \
            'max': self.dataset.y_spaces[task].bounds['max']}
        dico['title'] = prefix + "Goals chosen task " + str(task)
        dico['color'] = colors
        dico['marker'] = markers
        dico['linestyle'] = lines
        dico['legend'] = legends
        dico['plots'] = plots
        
        return dico
    
    def get_goals_mod_visualizer(self, task, prefix=""):
        """Return a dictionary used to visualize the goals on a specific task space chosen by mods."""
        cmap = plt.cm.jet
        colors = []
        markers = []
        lines = []
        legends = []
        plots = []
        for i in range(len(self.mods)):
            if len(self.mods) == 1:
                colors.append('k')
            else:
                colors.append(cmap(int(math.floor(float(i)*float(cmap.N - 1)/float(len(self.mods)-1)))))
            markers.append('.')
            lines.append('None')
            legends.append(self.mods[i].name)
            plots.append(lambda ax, options, i=i: self.plot_task_mod_goal(task, i, ax, options))
        
        dico = {}
        dico['limits'] = {'min': self.dataset.y_spaces[task].bounds['min'], \
            'max': self.dataset.y_spaces[task].bounds['max']}
        dico['title'] = prefix + "Goals chosen task " + str(task)
        dico['color'] = colors
        dico['marker'] = markers
        dico['linestyle'] = lines
        dico['legend'] = legends
        dico['plots'] = plots
        
        return dico


###### NEVER TESTED

class VisualizableAgent:
    """Learning agent that can be visualized in real time."""
    def __init__(self, learner, visualizers, t):
        """
        learner LearningAgentV4: the learner
        visualizers list of Visualizer: the visualizers that we want to observe at runtime
        t float list: the different steps to run learner and update visualization
        """
        self.learner = learner
        self.visualizers = visualizers
        self.t = t
    
    def run(self):
        """Run the visualizable learner on each step."""
        for t in self.t:
            self.learner.run(t)
            for v in self.visualizers:
                v.plot()



