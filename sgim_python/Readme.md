Python implementation of the SGIM algorithm.


Dependencies:

Numpy
Matplotlib
Scipy (>=0.18.1)
PoppyControl (git)

Utilization:

To run tests, launch the command:
./run_test.bash TEST_NAME N

To look at results from a test and plot results, use the script:
python visualize_bunches.py TEST_NAME1 TEST_NAME2 ...

where TESTNAME refers to the test folder (stored in data/) and N is the number of times you want to repeat the test

Package Structure:

The sources and scripts are contained in the root of the package.
A config folder contains configuration files to load easily some default environments, learners and evaluations.
The data folder contains the test config files and datafiles.
Config files must have the '.json' extension and testbenches must have a '.testbench' extension.
All teachers datasets are recorded in the teachers folder, and are referenced from this folder.
They are recorded in a DatasetV2 or SimpleDataset object and the file must have the '.data' extension.
Each test is stored in a folder named data/TEST_NAME and will contain the followings:
- config.json (gives the environment, learner, testbenches, evaluations)
- learner.json (describes the learner if the field is not in config.json)
- environment.json (describes environment if the field is not in config.json)
- evaluation.json (describes the evaluation if the field is not present in config.json)
- data/ (will contain the datafiles once the test ran)

Note:

When testing algorithms, always commit changes so that the test files could (not yet implemented) contains a version number (useful to understand how a test file was obtained).
Before launching tests, make sure a data folder exists within the test directory (there is sometimes bugs if the folder does not exist).
