#!/bin/bash


if [ "$#" -eq 0 ]
then
    echo "ERROR: Need the script file"
    exit 1
fi

PYTHON_SCRIPT=$1
shift

while test $# -gt 0
do
    python $PYTHON_SCRIPT $1 &
    shift
done