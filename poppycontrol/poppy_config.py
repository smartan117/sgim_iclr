import time
import math
import pypot.robot


def symetric2asymetric(config):
    """Pass between symetric and classic configuration of Poppy."""

    newConfig = {}
    newConfig['controllers'] = config['controllers']
    newConfig['motorgroups'] = config['motorgroups']
    motors = {}

    for m in config['motors']:
        motor = {}
        motor['type'] = config['motors'][m]['type']
        motor['id'] = config['motors'][m]['id']
        motor['angle_limit'] = config['motors'][m]['angle_limit']
        # On the symetric config, only right side joints not in y direction are concerned
        if m[0] == 'r' and m[len(m)-1] != 'y':
            if config['motors'][m]['orientation'] == "direct":
                motor['orientation'] = "indirect"
            else:
                motor['orientation'] = "direct"
            motor['offset'] = - config['motors'][m]['offset']
        else:
            motor['offset'] = config['motors'][m]['offset']
            motor['orientation'] = config['motors'][m]['orientation']
        motors[m] = motor
    
    newConfig['motors'] = motors

    return newConfig


def vrep_hack(robot):
    """Fix vrep orientation bug (from PoppyHumanoid)."""

    wrong_motor = [robot.r_knee_y, robot.abs_x, robot.bust_x]

    for m in wrong_motor:
        m.direct = not m.direct
        m.offset = -m.offset       
